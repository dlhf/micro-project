import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入 qiankun 中的 registerMicroApps 和 start 方法
import { registerMicroApps, start } from 'qiankun'

createApp(App).use(store).use(router).mount('#main')

// 注册子应用，一个对象就是一个子应用的配置
registerMicroApps([
    {
        name: 'admin',		// 子应用 package.json 中 name
        entry: '//localhost:8181',	// 子应用的 ip 地址
        // 子应用的容器，在主应用的 app.js 中，
        // 若为主应用，app.js 会获取 <router-view /> 的资源，
        // 若为子应用，app.js 会忽略 <router-view /> 中的资源，获取 id 为 container 的资源
        container: '#admin',
        activeRule: '/admin'		// 激活当前子应用时路径前自动拼接的路径
    }
])


// 如果需要进行拦截，可以在此处导航守卫设置，如果不需要，可以省略
const childrenPath = ['/admin']

router.beforeEach((to, from, next) => {
    // 如果有 name 属性，表示是主应用
    if (to.name) {
        next()
    }
    // 如果是子应用
    if (childrenPath.some((item) => to.path.includes(item))) {
        next();
    }
    // 如果没有当路由
    else {}
})

// 启动 qiankun
start()
