// 获取 package.json 的 name，可以直接写死，但是需要保持一致
const { name } = require('./package.json')

module.exports = {
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    port: 8181,
  },
  // 打包配置文件
  configureWebpack: {
    output: {
      library: `${name}-[name]`,
      libraryTarget: 'umd',	// 把微应用打包成 umd 库格式
      chunkLoadingGlobal: `webpackJsonp_${name}`
    },
  },
}
